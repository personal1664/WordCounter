package nl.ordina.wordcount;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.MediaType;

import java.util.*;

@ApplicationPath("rest")
public class WordFrequencyAnalyzerImpl extends Application implements WordFrequencyAnalyzer {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/calculateHighestFrequency")
    public int calculateHighestFrequency(@PathParam("text") String text) {
        return getHighestFrequencyFromMap(getWordFrequencyMap(text));
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/calculateFrequencyForWord")
    public int calculateFrequencyForWord(@PathParam("text") String text,
                                         @PathParam("word") String word) {
        //First '?i' modifier turns off case-sensitivity. '\b' modifier will only match whole words
        return text.split("(?i)\\b" + word).length - 1;
    }

    @GET
    @Path("/calculateMostFrequentNWords")
    public List<WordFrequency> calculateMostFrequentNWords(@PathParam("text") String text,
                                                           @PathParam("n") int n) {
        return getWordFrequencyMapByN(text, n);
    }

    private Map<String, Integer> getWordFrequencyMap(String text) {
        Map<String, Integer> wordFrequencyMap = new HashMap<>();
        // Create a stream of the provided text, split by everything except alphabetical letters, also filtering out empty text.
        // Afterwards, loop through every word and add an entry to the map of the lowercased word and increment the frequency
        Arrays.stream(text.split("[^A-z]")).filter(t -> !t.isEmpty())
                .forEach(word -> mergeIntoMap(wordFrequencyMap, word));
        return wordFrequencyMap;
    }

    private void mergeIntoMap(Map<String, Integer> map, String word) {
        map.merge(word.toLowerCase(), 1, Integer::sum);
    }

    private List<WordFrequency> getWordFrequencyMapByN(String text, int n) {
        List<WordFrequency> wordFrequencyList = new LinkedList<>();
        Map<String, Integer> wordFrequencyMap = getWordFrequencyMap(text);

        // Create a stream of the map entries containing all words and frequencies. First, sort in descending order by the value (frequency),
        // afterwards, sort in ascending alphabetical order by the key (word). Only add to the list if 'n' is not reached yet.
        wordFrequencyMap.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue(Comparator.reverseOrder()).thenComparing(Map.Entry.comparingByKey()))
                .forEachOrdered(x -> addToListIfLimitNotReached(wordFrequencyList, new WordFrequencyImpl(x.getKey(), x.getValue()), n));

        return wordFrequencyList;
    }

    private void addToListIfLimitNotReached(List<WordFrequency> wordFrequencies, WordFrequencyImpl wordFrequency, int n) {
        if(n > wordFrequencies.size())
            wordFrequencies.add(wordFrequency);
    }

    private int getHighestFrequencyFromMap(Map<String, Integer> wordFrequency) {
        // Find the max value (frequency) from the map and return it. 0 is returned if no entries are found in the map.
        Optional<Map.Entry<String, Integer>> highestFrequency = wordFrequency.entrySet().stream()
                .max(Map.Entry.comparingByValue());
        return highestFrequency.isPresent() ? highestFrequency.get().getValue() : 0;
    }
}
