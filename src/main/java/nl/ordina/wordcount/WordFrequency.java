package nl.ordina.wordcount;

public interface WordFrequency {
    String getWord();
    int getFrequency();
}