package nl.ordina.wordcount;

import org.junit.jupiter.api.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class WordFrequencyAnalyzerTest {
    WordFrequencyAnalyzerImpl wordFrequencyAnalyzer;

    @BeforeEach
    void setUp() {
        wordFrequencyAnalyzer = new WordFrequencyAnalyzerImpl();
    }

    /*
    calculateHighestFrequency tests
     */
    @Test
    @DisplayName("calculateHighestFrequency: Test with a simple sentence containing the word 'it' four times in different letter-casing")
    void testHighestWordFrequencyOfSentence() {
        assertEquals(4, wordFrequencyAnalyzer.calculateHighestFrequency("It works fine and is rather fast. " +
                "This is understandable because it seems to be a java-based solution, but since I don't really need iT, I'm going to leave it there."));
    }

    @Test
    @DisplayName("calculateHighestFrequency: Test with a simple sentence containing the word 'Java' three times including delimiter characters")
    void testHighestWordFrequencyOfSentenceWithDelimiters() {
        assertEquals(3, wordFrequencyAnalyzer.calculateHighestFrequency("Java works fine and is rather fast. " +
                "This is understandable because its a java-based solution, but since I don't really need them, going to leave the java.jee dependency there."));
    }

    @Test
    @DisplayName("calculateHighestFrequency: Test with a simple sentence containing the word 'Java' AND 'it' three times")
    void testHighestWordFrequencyOfSimpleSentenceDuplicateResult() {
        assertEquals(3, wordFrequencyAnalyzer.calculateHighestFrequency("Java works fine and is rather fast. " +
                "This is understandable because it is a java-based solution, but since I don't really need them, going to leave the java.jee dependency there."));
    }

    @Test
    @DisplayName("calculateHighestFrequency: Test that zero is returned if an empty string is provided")
    void testHighestWordFrequencyEmptyInput() {
        assertEquals(0, wordFrequencyAnalyzer.calculateHighestFrequency(""));
    }

    @Test
    @DisplayName("calculateHighestFrequency: Test that zero is returned if no words are provided")
    void testHighestWordFrequencyDelimiterInput() {
        assertEquals(0, wordFrequencyAnalyzer.calculateHighestFrequency(" - , 6 9 6"));
    }

    /*
     calculateFrequencyForWord tests
     */
    @Test
    @DisplayName("calculateFrequencyForWord: Test that the word 'it' has a frequency of four, also testing case-insensitivity")
    void testWordFrequencyForWordCaseInsensitivity() {
        assertEquals(4, wordFrequencyAnalyzer.calculateFrequencyForWord("It works fine and is rather fast. " +
                "This is understandable because it seems to be a java-based solution, but since I don't really need iT, I'm going to leave it there.", "it"));
    }

    @Test
    @DisplayName("calculateFrequencyForWord: Test that the word 'iT' also has a frequency of four")
    void testWordFrequencyForSuppliedWordCaseInsensitivity() {
        assertEquals(4, wordFrequencyAnalyzer.calculateFrequencyForWord("It works fine and is rather fast. " +
                "This is understandable because it seems to be a java-based solution, but since I don't really need IT, I'm going to leave it there.", "iT"));
    }

    @Test
    @DisplayName("calculateFrequencyForWord: Test that the word 'Ordina' has a frequency of zero")
    void testWordFrequencyForNonOccuringWord() {
        assertEquals(0, wordFrequencyAnalyzer.calculateFrequencyForWord("It works fine and is rather fast. " +
                "This is understandable because it seems to be a java-based solution, but since I don't really need iT, I'm going to leave it there.", "Ordina"));
    }

    @Test
    @DisplayName("calculateFrequencyForWord: Test that zero is returned if an empty string is provided")
    void testWordFrequencyEmptyInput() {
        assertEquals(0, wordFrequencyAnalyzer.calculateFrequencyForWord("", "bla"));
    }

    @Test
    @DisplayName("calculateFrequencyForWord: Test that zero is returned if no words are provided")
    void testWordFrequencyDelimiterInput() {
        assertEquals(0, wordFrequencyAnalyzer.calculateFrequencyForWord(" - , 6 9 6", "something"));
    }

    /*
    calculateMostFrequentNWords tests
    */
    @Test
    @DisplayName("calculateMostFrequentNWords: Test that the most frequent words are correctly determined and sorted by frequency and then alphabetically")
    void testWordFrequencyForMostFrequentNWords() {
        List<WordFrequency> wordFrequencyList = wordFrequencyAnalyzer.calculateMostFrequentNWords("It works fine and is rather fast. " +
                "This is understandable because it seems to be a java-based solution, but since I dont really need iT, I'm going to leave it there.", 7);
        assertEquals(7, wordFrequencyList.size());
        assertEquals(4, wordFrequencyList.get(0).getFrequency());
        assertEquals("it", wordFrequencyList.get(0).getWord());
        assertEquals(2, wordFrequencyList.get(1).getFrequency());
        assertEquals("i", wordFrequencyList.get(1).getWord());
        assertEquals(2, wordFrequencyList.get(2).getFrequency());
        assertEquals("is", wordFrequencyList.get(2).getWord());
        assertEquals(2, wordFrequencyList.get(3).getFrequency());
        assertEquals("to", wordFrequencyList.get(3).getWord());
        assertEquals(1, wordFrequencyList.get(4).getFrequency());
        assertEquals("a", wordFrequencyList.get(4).getWord());
        assertEquals(1, wordFrequencyList.get(5).getFrequency());
        assertEquals("and", wordFrequencyList.get(5).getWord());
        assertEquals(1, wordFrequencyList.get(6).getFrequency());
        assertEquals("based", wordFrequencyList.get(6).getWord());
    }

    @Test
    @DisplayName("calculateMostFrequentNWords: Test that all words are returned when specified 'n' limit is higher than the amount of words")
    void testWordFrequencyForMostFrequentNWordsHigherN() {
        List<WordFrequency> wordFrequencyList = wordFrequencyAnalyzer.calculateMostFrequentNWords("It works fine and is rather fast. " +
                "This is understandable because it seems to be a java-based solution, but since I dont really need iT, I'm going to leave it there.", 150);
        assertNotNull(wordFrequencyList);
        assertEquals(27, wordFrequencyList.size());
    }

    @Test
    @DisplayName("calculateMostFrequentNWords: Test that all words are returned when specified 'n' limit is higher than the amount of words")
    void testWordFrequencyForMostFrequentNWordsZeroN() {
        List<WordFrequency> wordFrequencyList = wordFrequencyAnalyzer.calculateMostFrequentNWords("It works fine and is rather fast. " +
                "This is understandable because it seems to be a java-based solution, but since I dont really need iT, I'm going to leave it there.", 0);
        assertNotNull(wordFrequencyList);
        assertEquals(0, wordFrequencyList.size());
    }

    @Test
    @DisplayName("calculateMostFrequentNWords: Test that an empty list is returned if an empty string is provided")
    void testWordFrequencyForMostFrequentNWordsEmptyInput() {
        assertEquals(0, wordFrequencyAnalyzer.calculateMostFrequentNWords("", -1).size());
    }

    @Test
    @DisplayName("calculateMostFrequentNWords: Test that an empty list is returned if no words are provided")
    void testWordFrequencyForMostFrequentNWordsDelimiterInput() {
        assertEquals(0, wordFrequencyAnalyzer.calculateMostFrequentNWords(" - , 6 9 6", 1).size());
    }
}
